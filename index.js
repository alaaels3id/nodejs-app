var mongodb = require('mongodb').MongoClient
var url = "mongodb://localhost:27017/";

mongodb.connect(url, {useNewUrlParser: true},function(err, db){
    if(err) throw err;
    console.log('Database created !');
    
    var dbase = db.db("mydb"); //here
    
    dbase.createCollection("customers", function(err, res) {
        if (err) throw err;
        console.log("Collection created!");
        db.close();   //close method has also been moved to client obj
    });
});
