var mongodb = require('mongodb').MongoClient
var url = "mongodb://localhost:27017/";

mongodb.connect(url, {useNewUrlParser: true},function(err, db){
    if(err) throw err;
    console.log('Database created !');
    
    var dbo = db.db("mydb"); //here
    var info = {name:"Alaa ELsaid", age:25, dob:"27-09-1994", address:"aga, dakahlia, egypt"};
    
    dbo.collection("persons").insertOne(info, function(err, result) {
        if (err) throw err;
        console.log("1 document inserted");
        db.close();   //close method has also been moved to client obj
    });
});